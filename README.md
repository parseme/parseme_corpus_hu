README
======
This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for Hungarian, edition 1.3. See the wiki pages of the [PARSEME corpora initiative](https://gitlab.com/parseme/corpora/-/wikis/home) for the full documentation of the annotation principles.

The present Hungarian data result from an enhancement of the Hungarian part of the [PARSEME corpus v 1.1](http://hdl.handle.net/11372/LRT-2842).
For the changes with respect to version 1.1, see the change log below.

Corpora
-------
All annotated data are legal texts coming from the Szeged Dependency Treebank. 

Provided annotations
--------------------
The data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Manually annotated.
* UPOS and XPOS (columns 4 and 5): Available. Automatically generated (UDPipe 2, model hungarian-szeged-ud-2.10-220711). The tagset is the one of [UD POS-tags](http://universaldependencies.org/u/pos).
* FEATS (column 6): Available. Automatically generated (UDPipe 2, model hungarian-szeged-ud-2.10-220711). The tagset is [UD features](http://universaldependencies.org/u/feat/index.html).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically generated (UDPipe 2, model hungarian-szeged-ud-2.10-220711). The tagset is [UD dependency relations](http://universaldependencies.org/u/dep).
* MISC (column 10): No-space information available. Automatically generated (UDPipe 2, model hungarian-szeged-ud-2.10-220711).
* PARSEME:MWE (column 11): Manually annotated by a single annotator per file. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: VID, LVC.full, LVC.cause, VPC.full, VPC.semi.
<!---
* UPOS (column 4): Available. Manually annotated. The tagset is MSD.
* FEATS (column 6): Available. Manually annotated. The tagset is MSD.
* HEAD and DEPREL (columns 7 and 8): Available. Manually annotated. The inventory is the original annotation of the Szeged Dependency Treebank.
* MISC (column 10): No-space information available. Automatically annotated.
--->
Tokenization
------------
Manually annotated for all the files.

Statistics
-------
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 


Authors
----------
All VMWEs annotations were performed by Viktória Kovács, Katalin Ilona Simkó and Veronika Vincze.


License
----------
The VMEs annotations are distributed under the terms of the [CC-BY v4](https://creativecommons.org/licenses/by/4.0/) license.
<!-- The lemmas, POS and morphological features, contained in CONNL-U files are distributed under the terms of the GNU General Public License v.3 ([GNU GPL v.3](https://www.gnu.org/licenses/gpl.html)). -->


Contact
----------
vinczev@inf.u-szeged.hu

Future work
----------
The source data come from the Szeged Dependency Treebank (SDT), which is manually annotated for morphosyntax. Therefore, columns 3-10 (LEMMA to MISC) should in the future be aligned with those stemming from the latest version of SDT and converted to UD. 

Change log
----------
- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - The manual morphosyntactic annotation (columns UPOS to MISC) from version 1.1 was not UD-compatible (it kept the POS and features from the original SDT treebank). Therefore, these columns were re-generated automatically with [UDPipe 2](https://ufal.mff.cuni.cz/udpipe/2/) (hungarian-szeged-ud-2.10-220711 model) by Agata Savary.
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.  
